## Background 

* In this section, there will be a couple Q/A type responses that are commonly asked during CLI or GIT training.   
* Understanding command line (and all related items) will help strengthen and speed up your understanding of GIT.   

**Disclaimer**: If the links below no longer work, are inaccurate, or is hard to follow, please feel free to make a commit to remove it from this training guide. As you go through this guide, please feel free to contribute or change anything below.   

### Command Line (and Interface)

*  What is the command line interface (CLI)? 
    *  [CLI is a text interface that passes `commands` to the computer operating system (OS).](https://www.codecademy.com/articles/command-line-commands) 
*  What is a command line? 
    *  [A line in the CLI that you write text into. It’s where the command is typed.](https://www.howtogeek.com/437682/command-lines-why-do-people-still-bother-with-them/) 
*  Why do people use command line? 
    *  [Short Answer: To do everything via keyboard](https://www.hostinger.com/tutorials/what-is-cli) 
    *  [Long Answer: “When you use programs with Graphical User Interfaces (GUI) certain buttons perform certain tasks on the computers. The Command Line Interface does that too, but it allows you to do it with more precision and power. You type words and hit enter, the shell interprets those words, and works with the OS kernel and files to execute the command.”](https://learntocodewith.me/getting-started/topics/command-line/) 
*  How can I access the command line interface for MacOS ?
    *  Terminal is an application (app) on the MacOS system that provides a command line interface.
*  What about for other operating systems? 
    *  [Unix/Linux](https://www.softwaretestinghelp.com/unix-vs-linux/): Terminal 
    *  Windows: Command Prompt 
*  What is the difference between Unix/Linux, Windows, and MacOS command line? 
    *  They have their sets of command line prompts. See `References` below for the standard list of command line prompts for MacOS.  
*  Are there any special terminal commands that are limited in permissions?
    *  [Administrator Account (Root User)](https://support.apple.com/en-us/HT202035) privileges are needed to run any command starting with `sudo`. Will require password. 
    *  Local User - your normal user account you run most commands through. 
*  How does this impact [file access and security](https://support.apple.com/guide/mac-help/change-permissions-for-files-folders-or-disks-mchlp1203/mac)? 
    *  [You can change folder, file, and other access permissions on a user, group, or others level that will let people read, write, or execute.](https://ss64.com/osx/chmod.html)
    *  [You can also change file owner](https://ss64.com/osx/chown.html)


### Shell / BASH 

*  What is `shell`? 
    *  Terminal, an application, can run the shell program on MacOS. 
    *  [Shell is a program that processes the commands and returns the output of the command.](https://askubuntu.com/questions/506510/what-is-the-difference-between-terminal-console-shell-and-command-line) 
*  What is an example of a shell script? 
    *  Any file that ends in .sh 
    *  [The `onboarding_script.sh` can be found in the Data Onboarding Issue](https://gitlab.com/gitlab-data/analytics/issues/2124) 
*  Now that you brought up the data onboarding guide, can you walk me through what the following commands mean? 
    *  `curl` is [a command line prompt that transfers data](https://curl.haxx.se). 
        *  In the example below, we are transferring the file from the gitlab-data repo from a remote URL to your local directory `/` as a file named `onboarding_script.sh`. 
    *   `bash` stands for “bourne-again shell”, so essentially a popular command-line shell (program that runs command line prompts) 
        *  In the example below, we are running the shell script `onboarding_script.sh` from the location `~/onboarding_script.sh`. 
    *  `rm` is a command line prompt that removes a content. 
        *  In the example below, we are removing the onboarding_script.sh from the `/` directory. 
    *   **Summary**: So to recap, we copy the shell script onto our local computer, we run the script, and then we delete the script. 
```
curl https://gitlab.com/gitlab-data/analytics/raw/master/admin/onboarding_script.sh > ~/onboarding_script.sh
bash ~/onboarding_script.sh
rm ~/onboarding_script.sh
```
*  What are BASH profiles? 
    *   ["BASH profiles are essentially shortcuts that we can create through aliases and shell functions using custom commands. While they aren’t required, they can help improve efficiency dependent on the shortcuts you create."](https://medium.com/swlh/bash-profiles-quick-tips-to-make-life-easier-a352f8b87c75)
    *   If you type `nano .bash_profile` in Terminal, you will see that the onboarding_script.sh that ran made modifications to your bash profile for Snowflake. 
*  In the `Data Team Onboarding Issue`, we use `goto analytics`. What does that do? 
    *   `goto` is a shell utility that registers a directory with an alias. 
    *   For `goto analytics`, it is saying "go to the directory that I have aliased as `analytics`". 
    *   [You can register and unregister directory alias. Click here for instructions.](https://github.com/iridakos/goto)
*  What else should I know about BASH? 
    *   [Other questions people have asked in relations to BASH profiles](https://www.stefaanlippens.net/bashrc_and_others/)
    *   [Git BASH for Windows](https://www.atlassian.com/git/tutorials/git-bash) - doesn't affect us since we're all on Macs but interesting to read. 

### File Directory System 
*  [Windows, Mac, and Linux have a file system that is organized in a tree-like structure.](https://www.codecademy.com/learn/learn-the-command-line/modules/learn-the-command-line-navigation)
*  [MacOS uses the Apple File System](https://en.wikipedia.org/wiki/Apple_File_System).
*  [The root directory `/` is the base of the tree for UNIX/Linux/MacOS.](https://en.wikipedia.org/wiki/Root_directory) 
    *  Note: Think of this as the ultimate directory (or folder) that contains all other directories (folders) and files. 
*  To move from one folder to another, you need to change directories or `cd`. 
*  If you put a dot before the forward slash `./`, [this will take you to a directory within the directory you are currently in](https://askubuntu.com/questions/358633/what-does-mean-in-linux-shell).
*  If you put a tilde before the forward slash `~/`, [the tilde will refer to your home directory](https://superuser.com/questions/158721/what-does-mean-in-terms-of-os-x-folders-directories).
*  Browse the internet for a list of unix/linux/macos commands for navigating file systems. 


### Programming Languages 
Anyone can learn any programming language as long as they have an interest and can dedicate time to learning it. 

*  How long does it take to get good at any programming language? 
    *   As long as it takes to learn any foreign language. 
    *   Practice (repitition) and understanding fundamental concepts will make it `look` like you are a whiz at any programming language. 
    *   [Click here to see how wikipedia classifies bash, shell, and python](https://en.wikipedia.org/wiki/List_of_programming_languages_by_type)
*  I see people typing the commands really quickly on tv shows, how long does it take to get that good? 
    *  If you practice typing the same command 10 times, you will be that good at typing it fast. 
*  What's the best programming language to learn? 
    *  Depends on what you are interested in achieving. Each language has its limitations and advantages. 


## References
*  [Standard list of command prompts for MacOS](https://ss64.com/osx/)
*  [Cheat sheet for command prompts](http://cheatsheetworld.com/programming/unix-linux-cheat-sheet/)
*  [Cheat sheet for shortcuts and core commands for MacOS](https://github.com/0nn0/terminal-mac-cheatsheet) 
*  [Goto alias registration/De-registration](https://github.com/iridakos/goto)
*  [Shell/BASH cheat sheet](https://gist.github.com/tmichel/7c54ac272cbca70fea1fbe9c1e0a4321)
*  [BASH/Shell scripting guide](http://matt.might.net/articles/bash-by-example/)
*  [101 Bash Commands and Tips for Beginners to Experts](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je)


## Exercises
For all the exercises below, you will need to open the Terminal application by searching `Terminal` on your Mac laptop.

### Understanding Command Line Prompts 
Run the following commands in the command line interface (CLI).

1. Check out where this application lives (will require admin priviledges) 
    1. To find it in a **specific directory** : `sudo find /System/Applications -iname "Terminal.app"`
    1. To find it **anywhere** on your Mac : `sudo find / -iname "Terminal.app”`
        *  The above command may run for a bit, so feel free to kill the command with `Ctrl + C`. 
1.  Use it to open another application 
    1. Ex: `open -a Grapher `
1. Clear the screen: `command + K` 
1. See and scroll through all command line prompts available in MacOS Terminal 
    1. Hit `esc` button twice. This will display `Display all 2483 possibilities? (y or n)`
    1. Hit `y` to see all the possibilities 
    1. Hit `return` to scroll through commands 
    1. Hit `delete` to exit this command listing 
1. Get more information for a terminal command 
    1. Hit `esc` button twice. This will display `Display all 2483 possibilities? (y or n)`
    1. Hit `y` to see all the possibilities 
    1. Highlight a command prompt. 
    1. Right click and select `open man page` (the typo is meant to be there). => There should be a pop-up window explaining that command.
    1. Hit `delete` to exit this command listing 

### Understanding BASH profile 
1. Check out your BASH profile by typing `nano .bash_profile` into the command line.
1. Update something in your BASH profile. [See example here.](https://natelandau.com/my-mac-osx-bash_profile/)

### Understanding Directory Aliases 
Run the following commands in the command line interface (CLI).

1. [Register a new alias for going to your desktop directory.](https://github.com/iridakos/goto#register-an-alias) 
    1. Type `goto -r desktop ~/Desktop` in the command line. You should see "Alias 'desktop' registered successfully." 
        * Note: If you type `goto desktop`, you should see something like this: "15:00:25 ~/Desktop $ ". This means you are now in the desktop directory.
        * Note: If you type `goto Desktop`, you will see "goto error: unregistered alias Desktop". These aliases are case sensitive.
1. [Unregister the new alias for going to your desktop directory.](https://github.com/iridakos/goto#unregister-an-alias) 
    1. Type `goto -u desktop` in the command line. You should see "Alias 'desktop' unregistered successfully."  
1. Feel free to create additional aliases. The `goto analytics` command is registered to go to the directory `~/repos/analytics/`

### Understanding File Directory System 
1. Go to your desktop. => Output: "15:32:24 ~/Desktop $ "
    1. `cd ~/Desktop`  
    1. `goto desktop` if you registered your alias 
1. List out all the folders/files on your desktop. 
    1. `ls` 
1. Let's navigate from the root directory. 
    1. Type `cd /` to change your directory to your root directory. 
    1. Type `ls` to list all the directories (folders) and files in the root directory. 
    1. Type `cd Users`. This should change the directory from `/` to `/Users`. 
    1. Type `ls`. => You should see your username there. 
    1. Type `cd ./ktam` or `cd ktam`. This will take you to your home directory. => Output: "15:41:27 ~ $"
    1. Type `cd /` to go back to the root directory. 
1. Go to your home directory. Check out what is there 
    1. Type `cd` or `cd ~`. You should see the same home directory that we just went to above. 
        * Note: You can also access this same directory using `cd /Users/ktam`.
    1. Type `ls`. You should see all the folders that are in your home directory. 
    1. Type `mkdir ./Desktop/food`. This will make a new directory (folder) on your desktop. 
    1. Check your desktop! 
1. Let's navigate back to your desktop by changing directories again to Desktop. 
    1. Type `cd ~/Desktop` or `goto desktop` if you registered your alias 
1. Create another folder on your desktop. 
    1. Type `mkdir drinks`. => Output: This will make a new directory on your desktop called "drinks". 
1. Let's add a file into the folder "food" called "fruits". 
    1. Type `atom food/fruit.txt` => Output: The atom UI should open with a file called fruit.txt. 
    1. In the Atom GUI, hit `Command + S` to save the file into that directory. 
    1. In the Atom GUI, type into the file at least 2 names of fruits each on their own lines. 
    1. In the Atom GUI, hit `Command + S` to save the changes to the file. 
    1. Using your mouse, go to the food directory and open the `fruits.txt` file. You should see the fruits in that file. 
    1. Close the Atom GUI and the file. Go back to Terminal. 
1. Let's edit that "fruit" file from Terminal. Go back to Terminal.
    1. Type `vi ~/Desktop/food/fruits.txt`. => This will open the text file called `fruits.txt` from the `food` folder on the Desktop.
        * Note: [vi is another text editor that has its own set of commands.](https://www.cs.colostate.edu/helpdocs/vi.html)
    1. Using the `down` key, go to the last row in the file. 
    1. Using the `right` key, go to the last character in that row. 
    2. Type `a`. This will allow you to append text to that file. 
    1. Start typing the names of other fruits into the file like you would normally type in a text file. 
    1. Hit `esc` to be able to use other vi text command prompts. 
    1. Type `:w` to save the changes to the file. 
    1. Type `:q` to exit the vi commands and go back to Terminal commands. 
    1. Check the file from your desktop.
1. Let's delete the food and the drinks folder. 
    1. In Terminal, type `rm food`. Output: An error since you can use `rm <file_name>` or `rm -r <directory_name>`. 
    1. Type `rm -r food`. This will recursively delete the folder and the file inside of it. 
        * Note: [Recursive just means it will delete what's inside the folder first and then the folder itself.](https://www.quora.com/What-does-delete-folders-files-recursively-means)
    1. Delete the drinks folder. 

### Understanding more advanced command line prompts 
* [Run through this training guide for BASH Commands](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je)
* [Explore grep and regular expression commands.](http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_04_02.html)
* Try out the `Periscope Query Bash Details` in the merge request template for `dbt model` changes and see if you can understand what it is asking for. 

## Food for Thought
In this guide, we've discussed some common BASH/shell commands. 
1. Type `git version` in terminal. How does the terminal know to use this command to return the git version on your mac? 
2. Try `python version`. Then try `python --version`. Which one worked and what does this tell us? 
3. Try `java --version` or `ruby -v`. Any results? Any thoughts about these three commands? 
4. [Create your own bash script and run your own commands](https://www.taniarascia.com/how-to-create-and-use-bash-scripts/)
